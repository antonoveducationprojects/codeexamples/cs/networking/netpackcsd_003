﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 22.10.2018
 * Time: 23:00
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace NetServer
{
	class Program
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Net Server is starting ....\nWait please ..........");
//			IPHostEntry ipHost = Dns.GetHostEntry("localhost");			
            Console.Write("Server listen IP: ");
            string serverIP= Console.ReadLine();
			IPAddress ipAddr = IPAddress.Parse(serverIP);//ipHost.AddressList[0];
            Console.Write("Server listen Port: ");			
            string serverPort= Console.ReadLine();            
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, int.Parse(serverPort));
            Socket sListener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                sListener.Bind(ipEndPoint);
                sListener.Listen(10); 
                while (true)
                {
	                Console.WriteLine("Waiting for connection on port {0}", ipEndPoint);
                    Socket handler = sListener.Accept();
                    string data = null;
                    byte[] bytes = new byte[1024];
                    int bytesRec = handler.Receive(bytes);
                    data += Encoding.UTF8.GetString(bytes, 0, bytesRec);
                    Console.Write("Got text: " + data + "\n\n");
                    string reply = "Thanks for the request " + data.Length.ToString()
                                                             + " symbols";
                    byte[] msg = Encoding.UTF8.GetBytes(reply);
                    handler.Send(msg);
                    if (data.IndexOf("<TheEnd>") > -1)
                    {
	                    Console.WriteLine("Conection closed by server.");
                        break;
                    }
                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Console.ReadLine();
            }
            
            
            
            
			Console.Write("Press any key to continue . . . ");
			Console.ReadKey(true);
		}
	}
}