﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 22.10.2018
 * Time: 23:04
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace NetClient
{
	class Program
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Net Client is starting ....\nWait please ..........");
			try
            {
				Console.Write("Server listen IP: ");
                string serverIP= Console.ReadLine();
			    IPAddress ipAddr = IPAddress.Parse(serverIP);//ipHost.AddressList[0];
                Console.Write("Server listen Port: ");			
                string serverPort= Console.ReadLine();                
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, int.Parse(serverPort));
                //connect ???
                SendMessage(ipAddr, ipEndPoint);
                //disconnect ???
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
            	Console.Write("Client is stopped \nPress any key to continue . . . ");
			    Console.ReadKey(true);
            }
        }

        static void SendMessage(IPAddress ipAddr, IPEndPoint ipEndPoint)
        {
        	//1. Prepare to trasfer message
            byte[] bytes = new byte[1024];
            Console.Write("Input message: ");
            string message = Console.ReadLine();
            byte[] msg = Encoding.UTF8.GetBytes(message);
            //2. Sending message
            Socket sender = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            sender.Connect(ipEndPoint);
            Console.WriteLine("Strarting conection to {0} ", sender.RemoteEndPoint.ToString());
            int bytesSent = sender.Send(msg);
            int bytesRec = sender.Receive(bytes);
            Console.WriteLine("\nResponse: {0}\n\n", Encoding.UTF8.GetString(bytes, 0, bytesRec));
            //3. Close connection
            sender.Shutdown(SocketShutdown.Both);
            sender.Close();			            
            //4. Check for exit
            if (message.IndexOf("<TheEnd>") == -1)
                SendMessage(ipAddr, ipEndPoint);
		}
	}
}